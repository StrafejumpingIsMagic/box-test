# Box Test #

This is a small program for testing movement mechanic ideas.

### Setup ###

#### Linux ####

Can be done in a terminal window.

```
#!bash
git clone https://StrafejumpingIsMagic@bitbucket.org/StrafejumpingIsMagic/box-test.git box-test
cd box-test
make
```

#### Windows ####

You will need mingw32 and msys. You will need to alter the LDFLAGS variable to contain: -lSDL2main -lSDL2.dll

#### Cross compile using GNU toolchain ####

As of 2017-02-23 this apparently doesn't work anymore.

Your Linux distribution will likely have mingw32 packages for gcc-c++ and may have SDL2 development packages. Install them and the following will likely work with few modifications. This should work on Fedora without modification.


```
#!bash
make CXX=i686-w64-mingw32-g++ LDFLAGS="-mwindows -static-libgcc -static-libstdc++ -static -lmingw32 -lSDL2main -lSDL2.dll" PROJ_NAME="p1.exe"
```

```
#!bash
make CXX=x86_64-w64-mingw32-g++ LDFLAGS="-mwindows -static-libgcc -static-libstdc++ -static -lmingw32 -lSDL2main -lSDL2.dll" PROJ_NAME="p1.exe"
```

### Dependencies ###

GNU Make, GNU C++ compiler, SDL2 headers and library.

Clang might work, change CXX variable to try it.

### Keys ###

WASD for movement.

T sets the time scale to default.
Y sets the time scale to quarter speed.

J, jumps.

Escape key quits.