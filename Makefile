PROJ_NAME=p1
CXX=g++

CXXFLAGS=-c -O0 -g -Wall -Wextra -Werror -pedantic -std=c++11 -I./
LDFLAGS=-lSDL2

ENGINE_SOURCES= \
	main.cxx

SOURCES= \
	$(ENGINE_SOURCES) \

OBJS=$(SOURCES:.cxx=.o)

all: $(PROJ_NAME)

%.o: %.cxx
	$(CXX) $(CXXFLAGS) $< -o $@

$(PROJ_NAME): $(OBJS)
	$(CXX) $(OBJS) $(LDFLAGS) -o $@

.PHONY: clean
clean:
	rm -v $(PROJ_NAME) $(OBJS)

.PHONY: info
info:
	@echo "Objects: $(OBJS)"
