#include <vector>
#include <iostream>
#include <SDL2/SDL.h>

/*
 * sdl
 */

SDL_Window* window;
SDL_Renderer* renderer;
Uint8* keyState;

/*
 * options
 */

double timescale = 1.0;
double physics_hz = 60;
double render_hz = 120;
bool frameskip = true;

/*
 * vec
 */

typedef double vec;
typedef struct vec2 vec2;
struct vec2 {
	vec x;
	vec y;
	void flip() {
		x = -x;
		y = -y;
	};
	void scale(vec s) {
		x *= s;
		y *= s;
	}
	vec length() {
		return sqrt( (x * x) + (y * y) );
	}
	void resize(vec in_sz) {
		vec mag = length();
		if (x != 0) {
			x /= mag;
		} else {
			x = 0;
		}
		if (y != 0) {
			y /= mag;
		} else {
			y = 0;
		}
		scale(in_sz);
	}
};
typedef struct box2 box2;
struct box2 {
	vec2 pos;
	vec2 size;
};

constexpr vec pixels(double v) {
	return (vec)v; // 1:1
}

vec clampVec(vec in_vec, vec min, vec max) {
	if (in_vec > max) return max;
	if (in_vec < min) return min;
	return in_vec;
}

bool boxTrace(box2& a, box2& b) {
	if (
		a.pos.x + a.size.x < b.pos.x ||
		a.pos.x > b.pos.x + b.size.x ||
		a.pos.y + a.size.y < b.pos.y ||
		a.pos.y > b.pos.y + b.size.y
	) {
		return false;
	}
	return true;
}

vec2 boxCenter(box2& b) {
	return { b.pos.x + (b.size.x / 2), b.pos.y + (b.size.y / 2) };
}

/*
 * weird entity type
 */

typedef struct cbox cbox;
struct cbox {
	box2 box;
	vec2 vel;
	bool grounded;
	bool active;
};

/*
 * Repel a box from another box.
 */

void boxRepel(cbox& mover, cbox& other) {
	box2& a = mover.box;
	box2& b = other.box;
	if ( boxTrace(a, b) ) {
		// find closest edges
		vec topdiff = (a.pos.y + a.size.y) - (b.pos.y);
		vec bottomdiff = (a.pos.y) - (b.pos.y + b.size.y);
		vec leftdiff = (a.pos.x + a.size.x) - (b.pos.x);
		vec rightdiff = (a.pos.x) - (b.pos.x + b.size.x);
		if (topdiff < 0) topdiff = -topdiff;
		if (bottomdiff < 0) bottomdiff = -bottomdiff;
		if (leftdiff < 0) leftdiff = -leftdiff;
		if (rightdiff < 0) rightdiff = -rightdiff;
		if (topdiff < bottomdiff && topdiff < leftdiff && topdiff < rightdiff) {
			// repel upwards
			a.pos.y = b.pos.y - (a.size.y + 1);
			mover.grounded = true;
			if (mover.vel.y > 0) mover.vel.y = 0;
		} else if (bottomdiff < topdiff && bottomdiff < leftdiff && bottomdiff < rightdiff) {
			// repel downwards
			a.pos.y = b.pos.y + b.size.y + 1;
			if (mover.vel.y < 0) mover.vel.y = 0;
		} else if (leftdiff < rightdiff && leftdiff < topdiff && leftdiff < bottomdiff) {
			// repel left
			a.pos.x = b.pos.x - (a.size.x + 1);
			if (mover.vel.x > 0) mover.vel.x = 0;
		} else if (rightdiff < leftdiff && rightdiff < topdiff && rightdiff < bottomdiff) {
			// repel right
			a.pos.x = b.pos.x + b.size.x + 1;
			if (mover.vel.x < 0) mover.vel.x = 0;
		} else {
			// no closest edge, move a pixel upwards and try again
			a.pos.y -= 1; boxRepel(mover, other);
		}
	}
}


/*
 * Gravity
 */

vec gravityAccel = pixels(2000);
vec gravityMaxFallSpeed = pixels(8000);

void applyGravity(cbox& box, double tsdelta) {
	if (box.vel.y < gravityMaxFallSpeed && box.grounded == false) box.vel.y += gravityAccel * tsdelta;
}

/*
 * Screen bounds
 */

bool isBoxOutOfBounds(box2& box) {
	if (
		box.pos.x + box.size.x > 640 ||
		box.pos.x < 0 ||
		box.pos.y + box.size.y > 480 ||
		box.pos.y < 0
	) {
		return true;
	}
	return false;
}

void boxBoundsClamp(box2& box) {
	if (box.pos.x + box.size.x > 640) box.pos.x = 640 - box.size.x;
	if (box.pos.y + box.size.y > 480) box.pos.y = 480 - box.size.y;
	if (box.pos.x < 0) box.pos.x = 0;
	if (box.pos.y < 0) box.pos.y = 0;
}

/*
 * Used to rate control and lerp physics frames.
 */

double acc = 0.0;
double render_lerp = 0.0;

/*
 * Collision Boxes
 */


std::vector<cbox> previousFrame;

// the boxes, first is the player box
std::vector<cbox> boxes = {
	{ { {0, 0}, {32, 32} }, {0, 0}, false, true },
	{ { {0, 400}, {640, 32} }, {0, 0}, false, false }, 
	{ { {128, 340}, {64, 96} }, {0, 0}, false, false }, 
	{ { {256, 300}, {64, 32} }, {0, 0}, false, false }, 
	{ { {400, 260}, {96, 100} }, {0, 0}, false, false }, 
	{ { {500, 200}, {64, 32} }, {0, 0}, false, false }, 
	{ { {0, 100}, {16, 32} }, {0, 0}, false, false }, 
	{ { {128, 120}, {16, 12} }, {0, 0}, false, false }, 
	{ { {192, 140}, {32, 16} }, {0, 0}, false, false }, 
	{ { {200, 150}, {12, 16} }, {0, 0}, false, false }, 
	{ { {300, 160}, {32, 12} }, {0, 0}, false, false }, 
	{ { {400, 180}, {16, 12} }, {0, 0}, false, false }
};

void boxGroundMove(cbox& e, unsigned index) {
	// test if we can fall, if so then we aren't grounded anymore
	e.box.pos.y += 1;
	bool blocked = false;
	for (unsigned j = 0; j < boxes.size(); j++) {
		if ( boxTrace(boxes[index].box, boxes[j].box) && index != j ) {
			blocked = true;
		}
	}
	e.box.pos.y -= 1;
	if (blocked == false) e.grounded = false;
}

void boxPhysics(double tsdelta) {
	for (unsigned i = 0; i < boxes.size(); i++) {
		if (boxes[i].grounded == true) {
			boxGroundMove(boxes[i], i);
		}
		if (boxes[i].active == true) applyGravity(boxes[i], tsdelta);
		boxes[i].box.pos.x += boxes[i].vel.x * tsdelta;
		boxes[i].box.pos.y += boxes[i].vel.y * tsdelta;

		if ( isBoxOutOfBounds(boxes[i].box) ) {
			boxBoundsClamp(boxes[i].box);
		}
		for (unsigned j = 0; j < boxes.size(); j++) {
			if ( boxTrace(boxes[i].box, boxes[j].box) && i != j ) {
				if (boxes[i].active == true) boxRepel(boxes[i], boxes[j]);
			}
		}
	}
}

void drawBoxes() {
	SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
	for ( unsigned i = 0; i < boxes.size(); i++ ) {
		cbox& c = boxes[i];
		cbox& o = previousFrame[i];
		SDL_Rect tmp = {
			(int)( ( o.box.pos.x * (1.0 - render_lerp) ) + ( c.box.pos.x * render_lerp ) + 0.5 ),
			(int)( ( o.box.pos.y * (1.0 - render_lerp) ) + ( c.box.pos.y * render_lerp ) + 0.5 ),
			(int)( ( o.box.size.x * (1.0 - render_lerp) ) + ( c.box.size.x * render_lerp ) + 0.5 ),
			(int)( ( o.box.size.y * (1.0 - render_lerp) ) + ( c.box.size.y * render_lerp ) + 0.5 )
		};
		SDL_RenderDrawRect(renderer, &tmp);
	}
}

/*
 * Player
 */

cbox& player = boxes[0];

vec playerRunSpeed = pixels(400);
vec playerRunAccel = pixels(1200);
vec playerFriction = pixels(1000);
vec playerBounceScale = 0.1;
vec jumpVelocity = pixels(5000);
vec jumpVelocityDecay = pixels(18000);
vec jumpVelocityCurrent = jumpVelocity;
bool jumpLock = false;

void drawPlayer() {
	cbox& c = boxes[0];
	cbox& o = previousFrame[0];
	SDL_Rect tmp = {
		(int)( ( o.box.pos.x * (1.0 - render_lerp) ) + ( c.box.pos.x * render_lerp ) + 0.5 ),
		(int)( ( o.box.pos.y * (1.0 - render_lerp) ) + ( c.box.pos.y * render_lerp ) + 0.5 ),
		(int)( ( o.box.size.x * (1.0 - render_lerp) ) + ( c.box.size.x * render_lerp ) + 0.5 ),
		(int)( ( o.box.size.y * (1.0 - render_lerp) ) + ( c.box.size.y * render_lerp ) + 0.5 )
	};
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderDrawRect(renderer, &tmp);
}

// contrary to the name, doesn't actually move the player. instead sets up velocities based on input
void movePlayer(double tsdelta) {
	if ( isBoxOutOfBounds(player.box) ) {
		boxBoundsClamp(player.box);
	}

	// the longer are jumping, the less the jetpack-ish effect becomes
	if (jumpVelocityCurrent > 0) {
		jumpVelocityCurrent -= jumpVelocityDecay * tsdelta;
		if (jumpVelocityCurrent < 0) jumpVelocityCurrent = 0;
	}

	// move left, right or not
	if ( keyState[SDL_SCANCODE_A] ) {
		if (player.vel.x > -playerRunSpeed) player.vel.x -= playerRunAccel * tsdelta;
	} else if ( keyState[SDL_SCANCODE_D] ) {
		if (player.vel.x < playerRunSpeed) player.vel.x += playerRunAccel * tsdelta;
	} else {
		if (player.grounded == true) {
			player.vel.resize(
				clampVec(
					player.vel.length() - (playerFriction * tsdelta),
					0,
					player.vel.length()
				)
			);
		}
	}

	// jump
	if (keyState[SDL_SCANCODE_J]) {
		if (player.grounded == true && !jumpLock) {
			jumpVelocityCurrent = jumpVelocity;
			jumpLock = true;
		}
		player.vel.y -= jumpVelocityCurrent * tsdelta;
	} else {
		jumpLock = false;
	}

	// timescale shortcuts
	if (keyState[SDL_SCANCODE_T]) {
		timescale = 1.0;
	} else if (keyState[SDL_SCANCODE_Y]) {
		timescale = 0.25;
	}
}


void gameLoop(double tsdelta) {
	acc += tsdelta;
	while (acc > 1/physics_hz) {
		previousFrame = boxes;
		movePlayer(1/physics_hz);
		boxPhysics(1/physics_hz);
		acc -= 1/physics_hz;
		if (!frameskip) break;
	}
	render_lerp = acc / (double)(1/physics_hz);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
	SDL_RenderClear(renderer);
	drawBoxes();
	drawPlayer();
}

int main(int, char**) {
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("proto1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, 0);
	keyState = (Uint8*)SDL_GetKeyboardState(NULL);
	previousFrame = boxes; // set the previous to the current, otherwise it won't exist
	Uint32 ptc = SDL_GetTicks();
	Uint32 ctc = ptc + (1000 / render_hz);
	Uint32 td = 0;
	while (keyState[SDL_SCANCODE_ESCAPE] != 1) {
		td = ctc - ptc;
		while (td < 1000/render_hz) {
			SDL_Delay( (1000/render_hz) - td );
			ctc = SDL_GetTicks();
			td = ctc - ptc;
		}

		double tsdelta = (double)(td) / 1000;

		SDL_PumpEvents();
		gameLoop(tsdelta * timescale);
		SDL_RenderPresent(renderer);

		td -= 1000/render_hz;
		ptc = ctc;
	}
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
